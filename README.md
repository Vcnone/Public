<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/4.7.0/css/font-awesome.css">
	<style type="text/css">
		img{
			border-radius: 10%;
			overflow: hidden;
			width: 200px;
		}
	</style>
</head>

<body>
<div align="center">
	<img src="https://portrait.gitee.com/uploads/avatars/user/2533/7600995_Vcnone_1652776597.png!avatar200" height="200px" width="200px "/>
</div>

<center>
	<div class="list-group">
	<a href="https://vcnone.top" class="list-group-item"><i class="fa fa-home fa-2x fa-fw"></i></a>
	<a href="mailto:wo@vcnone.top" class="list-group-item"><i class="fa fa-envelope-o fa-2x fa-fw"></i></a>
	<a href="https://t.me/vcnone_bot" class="list-group-item"><i class="fa fa-telegram fa-2x fa-fw"></i></a>
	<a href="https://github.com/Vcnone" class="list-group-item"><i class="fa fa-github fa-2x fa-fw"></i></a>
	</div>
</center>
</body>

</html>
